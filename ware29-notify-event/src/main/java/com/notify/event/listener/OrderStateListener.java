package com.notify.event.listener;

import com.notify.event.entity.OrderStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 订单变化监听
 * @author 公众号:知了一笑
 * @since  2022-04-23 19:58
 */
@Component
public class OrderStateListener implements ApplicationListener<OrderStateEvent> {

    private static final Logger logger = LoggerFactory.getLogger(OrderStateListener.class) ;

    @Async
    @Override
    public void onApplicationEvent(OrderStateEvent orderStateEvent) {
        logger.info(Thread.currentThread().getName()+";"+orderStateEvent);
    }
}
